﻿using System.Collections.Generic;
using Text_CodeFirst.Models;
using Text_CodeFirst.UnitOfwork;
using Text_CodeFirst.Repository;

namespace Text_CodeFirst.Service
{
    public class CustomerService : ICustomerService
    {
        private ICustomerUnitOfwork _unitOfwork;

        public CustomerService(ICustomerUnitOfwork unitOfwork)
        {
            _unitOfwork = unitOfwork;
        }
        
        public Customer GetById(int? id)
        {
            return _unitOfwork.CustomerRepository.GetById(id);
        }

        public IEnumerable<Customer> GetAll()
        {
            return _unitOfwork.CustomerRepository.GetAll();
        }

        public void Add(Customer customer)
        {
            CustomerUnitOfwork.CustomerRepository.Add(customer);
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Customer customer)
        {
            throw new System.NotImplementedException();
        }

        public void save()
        {
            throw new System.NotImplementedException();
        }
    }
}