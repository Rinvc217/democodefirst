﻿using System.Collections.Generic;
using Text_CodeFirst.Models;

namespace Text_CodeFirst.Service
{
    public interface ICustomerService
    {
        Customer GetById(int? id);
        IEnumerable<Customer> GetAll();
        void Add(Customer customer);
        void Delete(int id);
        void Update(Customer customer);
        void save();
    }
}