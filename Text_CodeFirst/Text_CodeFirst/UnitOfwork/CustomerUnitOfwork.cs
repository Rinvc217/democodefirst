﻿using System;
using Text_CodeFirst.Models;
using Text_CodeFirst.Repository;

namespace Text_CodeFirst.UnitOfwork
{
    public class CustomerUnitOfwork : ICustomerUnitOfwork
    {
        private CustomerContext _context;

        public CustomerUnitOfwork(CustomerContext context)
        {
            _context = context;
            InitCustomerRepository();
        }

        private bool _disposed = false;

//Dispose ở đây là để dọn dẹp những dữ liệu mà cần phải được dọn dẹp một cách đặc biệt
//ở trong object đó, chứ bản thân object đó vẫn tồn tại sau đó (Garbage Collector sẽ làm phần còn lại).        
        
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

 ///// (tham khảo Dispose: => https://daynhauhoc.com/t/khong-dung-using-trong-function/61637/3)        
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public static ICustomerRepository CustomerRepository { get; private set; }
        
        private void InitCustomerRepository()
        {
            CustomerRepository = new CustomerRepository(_context);
        }


        public void save()
        {
            _context.SaveChanges();
        }

        
    }
}