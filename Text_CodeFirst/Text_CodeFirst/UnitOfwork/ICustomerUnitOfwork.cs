﻿using System;
using Text_CodeFirst.Models;
using Text_CodeFirst.Repository;

namespace Text_CodeFirst.UnitOfwork
{
    public interface ICustomerUnitOfwork : IDisposable
    
//// Disposable có thể hiểu là giải phóng bộ nhớ
/// (tham khảo Dispose=> https://daynhauhoc.com/t/khong-dung-using-trong-function/61637/3)   
    {
        ICustomerRepository CustomerRepository { get; }
        void save();
    }
}