﻿using System;

namespace Text_CodeFirst.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public  string Name { get; set; }
        public  string Addr { get; set; }
        public Boolean Gender { get; set; }
    }
}