﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Text_CodeFirst.Models;

namespace Text_CodeFirst.Repository
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private CustomerContext context;
        private DbSet<Customer> _dbSet;
        private IEnumerable<Customer> _dbset;
        private bool disposed;

        public CustomerRepository(CustomerContext context)
        {
            this.context = context;
            _dbSet = context.Set<Customer>();
        }
 
//IEnumerable giúp các bạn thao tác với các collection in-memory sẽ tốt hơn.
//collection in-memory (bộ sưu tập trong bộ nhớ). 
        public IEnumerable<Customer> GetCustomers()
        {
            return _dbSet.ToList();
        }

        public Customer GetCustomerByID(int id)
        {
            return _dbSet.Find(id);
        }

        public void InsertCustomer(Customer customer)
        {
            _dbSet.Add(customer);
        }

        public void DeleteCustomer(int CustomerID)
        {
            Customer customer = context.Customers.Find(CustomerID);
            context.Customers.Remove(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            context.Entry(customer).State = EntityState.Modified;
     //context.Entry(entity).State = EntityState.Modified . Dòng này dùng để set state là edit đối tượng   
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public Customer GetById(int? id)
        {
            return context.Customers.Find(id);
        }

        public IEnumerable<Customer> GetAll()
        {
            return _dbset;
        }

        public void Add(Customer customer)
        {
            _dbset.Add
        }


        

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }

            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
//Dispose.SuppressFinalize là phương thức của garbage collector dùng để giải phóng bộ nhớ, hủy 1 số các object thừa. Ngắn gọn là thế
        }
    }
}