﻿using System.Collections.Generic;
using Text_CodeFirst.Models;

namespace Text_CodeFirst.Repository
{
    public interface ICustomerRepository 
    {
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerByID(int studentID);
        void InsertCustomer(Customer customer);
        void DeleteCustomer(int CustomerID);
        void UpdateCustomer(Customer customer);
        void Save();
        Customer GetById(int? id);
        IEnumerable<Customer> GetAll();
        void Add(Customer customer);
    }
}