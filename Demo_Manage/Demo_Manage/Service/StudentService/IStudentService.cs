﻿using System;
using System.Collections.Generic;
using Demo_Manage.Models;

namespace Demo_Manage.Service.StudentService
{
    public interface IStudentService
    {
        Student GetById(int id);
        IEnumerable<Student> GetAll();
        void Add(Student student);
        void Update(Student student);
        void Delete(int id);
        void save();
    }
}