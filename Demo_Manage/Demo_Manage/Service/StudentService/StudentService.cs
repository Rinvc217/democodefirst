﻿using System.Collections.Generic;
using Demo_Manage.Models;
using Demo_Manage.UnitOfWork;

namespace Demo_Manage.Service.StudentService
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private ContextDB _context;

        public StudentService(IUnitOfWork unitOfWork, ContextDB context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        
        public Student GetById(int id)
        {
            return _unitOfWork.StudentReponsitory.GetById(id);
        }

        public IEnumerable<Student> GetAll()
        {
            return _unitOfWork.StudentReponsitory.GetAll();
        }

        public void Add(Student student)
        {
            _unitOfWork.StudentReponsitory.Add(student);
        }
    
        public void Update(Student student)
        {
            _unitOfWork.StudentReponsitory.Update(student);
        }

        public void Delete(int id)
        {
            _unitOfWork.StudentReponsitory.Delete(id);
        }

        public void save()
        {
            _unitOfWork.StudentReponsitory.Save();
        }
    }
}