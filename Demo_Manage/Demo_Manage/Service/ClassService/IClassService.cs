﻿using System.Collections.Generic;
using Demo_Manage.Models;

namespace Demo_Manage.Service.ClassService
{
    public interface IClassService
    {
            Class GetById(int id);
            IEnumerable<Class> GetAll();
            void Add (Class clas);
            void Update(Class clas);
            void Delete(int id);
            void Save();
    }
}