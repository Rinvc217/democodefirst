﻿using System.Collections.Generic;
using Demo_Manage.Models;
using Demo_Manage.UnitOfWork;

namespace Demo_Manage.Service.ClassService
{
    public class ClassService : IClassService
    {
  //Constant, ReadOnly: là 1 dạng biến mà giá trị không thể thay đổi.
        
  //Constant:là giá trị của nó không thể thay đổi trong suốt chương trình.
  //Giá trị của nó sẽ có hiệu lực tại thời điểm biên dịch (compile - time).
        
 //ReadOnly:Readonly cũng tương tự như Constant nhưng điểm khác biệt là       
  //các bạn có thể thay đổi giá trị của nó nhưng chỉ được phép thay đổi trong Contructor mà thôi.
        private readonly IUnitOfWork _unitOfWork;
        private ContextDB _context;

        public ClassService(IUnitOfWork unitOfWork, ContextDB context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }
        
        public Class GetById(int id)
        {
            return _unitOfWork.ClassReponsitory.GetById(id);
        }

        public IEnumerable<Class> GetAll()
        {
            return _unitOfWork.ClassReponsitory.GetAll();
        }

        public void Add(Class clas)
        {
            _unitOfWork.ClassReponsitory.Add(clas);
        }

        public void Update(Class clas)
        {
            _unitOfWork.ClassReponsitory.Update(clas);
        }

        public void Delete(int id)
        {
            _unitOfWork.ClassReponsitory.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.ClassReponsitory.Save();
        }
    }
}