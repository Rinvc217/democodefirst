﻿using Demo_Manage.Models;
using Demo_Manage.Reponsitory.ClassReponsitory;
using Demo_Manage.Reponsitory.GenericReponsitory;
using Demo_Manage.Reponsitory.StudentReponsitory;
using Demo_Manage.Service.ClassService;
using Demo_Manage.Service.StudentService;
using Demo_Manage.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace Demo_Manage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // (Phương thức này được gọi bởi thời gian chạy. Sử dụng phương pháp này để thêm dịch vụ vào container).
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
    // Lambda này xác định xem có cần sự đồng ý của người dùng đối với cookie không cần thiết cho một yêu cầu cụ thể không.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<ContextDB>
            (
                options =>
                    options.UseSqlServer(
                        Configuration.GetConnectionString("ConnectDB")
                    )
            );  
 // *;phiên bản .NET Core trở đi, Microsoft đã hỗ trợ Dependency Injection(DI).
//Để sử dụng DI, bạn cần định nghĩa các service muốn sử dụng trong hàm ConfigureServices() trong class Startup.cs.
            
     //  Dependency injection()DI. hay tiêm phụ thuộc.
     // Transient: Đối tượng luôn được tạo mới khi có request từ Controller và từ Service.( mỗi lần gọi tới nó sẽ là 1 biến thể mới hoàn toàn )
            
     //Scoped: đối tượng được tạo ra khi có yêu cầu cho mỗi request.Trong cùng 1 request, nó sẽ chỉ được tạo 1 lần duy nhất
            
     //Singleton: đối tượng được tạo ra cho lần request đầu tiên. Sau đó các request tiếp theo,
     // sẽ trả về cùng 1 đối tượng. Nó sẽ được tạo lần đầu tiên khi được gọi, và sẽ sống quài cho tới khi app bạn chết
            services.AddTransient<IGenericReponsitory<Student>, GenericReponsitory<Student>>();
            services.AddTransient<IGenericReponsitory<Class>, GenericReponsitory<Class>>();
            services.AddTransient<IStudentService, StudentService>();
            services.AddTransient<IClassService, ClassService>();
            services.AddTransient<IStudentReponsitory, StudentReponsitory>();       
            services.AddTransient<IClassReponsitory, ClassReponsitory>();
            services.AddTransient<IUnitOfWork, UnitOfWork.UnitOfWork>();
        }

        // Phương thức này được gọi bởi thời gian chạy. Sử dụng phương pháp này để định cấu hình đường dẫn yêu cầu HTTP.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

