﻿using System.Collections.Generic;
using System.Xml.Linq;
using Demo_Manage.Models;
using Microsoft.EntityFrameworkCore;


namespace Demo_Manage.Reponsitory.GenericReponsitory
{
    public class GenericReponsitory<TEntity> : IGenericReponsitory<TEntity> where TEntity : class
    {
        private readonly ContextDB _context;
        private DbSet<TEntity> _dbSet;

        public GenericReponsitory(ContextDB context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }
        
        public TEntity GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
            _context.SaveChanges();
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}