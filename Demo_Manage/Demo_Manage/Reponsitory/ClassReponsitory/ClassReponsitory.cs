﻿using Demo_Manage.Models;
using Demo_Manage.Reponsitory.GenericReponsitory;

namespace Demo_Manage.Reponsitory.ClassReponsitory
{
    public class ClassReponsitory : GenericReponsitory<Class>, IClassReponsitory
    {
        public ClassReponsitory(ContextDB context) : base(context)
        {
        }
    }
}