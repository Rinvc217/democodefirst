﻿using Demo_Manage.Models;
using Demo_Manage.Reponsitory.GenericReponsitory;

namespace Demo_Manage.Reponsitory.StudentReponsitory
{
    public class StudentReponsitory : GenericReponsitory<Student>, IStudentReponsitory
    {
        public StudentReponsitory(ContextDB context) : base(context)
        {
        }
    }
}