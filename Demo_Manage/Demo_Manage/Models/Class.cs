﻿namespace Demo_Manage.Models
{
    public class Class
    {
        public int ClassID { get; set; }
        public string NameClass { get; set; }
        public string Faculty { get; set; }
    }
}