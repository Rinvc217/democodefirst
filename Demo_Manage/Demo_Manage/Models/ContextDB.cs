﻿using Microsoft.EntityFrameworkCore;

namespace Demo_Manage.Models
{
    public class ContextDB : DbContext
    {
        public ContextDB(DbContextOptions<ContextDB> options) : base(options)
        {
            
        }
        public DbSet<Class>Classes { get; set; }
        public DbSet<Student>Students { get; set; }  
    }
}
//EntitySet: DbContext chứa tập thực thể (DbSet<TEntity>) cho tất cả thực thể nối với những bảng của CSDL.