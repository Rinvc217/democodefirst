﻿namespace Demo_Manage.Models
{
    public class Student
    {
        public int StudentID { get; set; }
        public string NameStudent { get; set; }
        public string Addr { get; set; }
    }
}