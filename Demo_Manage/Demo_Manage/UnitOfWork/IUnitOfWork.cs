﻿using System;
using Demo_Manage.Models;
using Demo_Manage.Reponsitory.GenericReponsitory;

namespace Demo_Manage.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericReponsitory<Student> StudentReponsitory { get; }
        IGenericReponsitory<Class> ClassReponsitory { get; }
        void save();
    }
}
//Unit of Work chỉ có 1 nhiệm vụ duy nhất, đảm bảo tất cả các repository của bạn đều dùng chung một DbContext.
//Bằng cách này, khi thực hiện xong tất cả các tác vụ thay đổi database,
//bạn chỉ cần gọi DbContext.SaveChanges() 1 lần duy nhất, và các thay đổi đó sẽ được lưu lại trong database.