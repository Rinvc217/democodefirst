﻿using System;
using Demo_Manage.Models;
using Demo_Manage.Reponsitory.GenericReponsitory;

namespace Demo_Manage.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ContextDB _context;
        

        public UnitOfWork(ContextDB context)
        {
            _context = context;
            InitReponsitory();
        }

        private void InitReponsitory()
        {
            StudentReponsitory = new GenericReponsitory<Student>(_context);
            ClassReponsitory = new GenericReponsitory<Class>(_context);
        }
        
        private bool _disposed = false;
        private IUnitOfWork _unitOfWork;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {    
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericReponsitory<Student> StudentReponsitory { get; set; }
        public IGenericReponsitory<Class> ClassReponsitory { get; set; }

        public void save()
        {
            _context.SaveChanges();
        }
    }
}