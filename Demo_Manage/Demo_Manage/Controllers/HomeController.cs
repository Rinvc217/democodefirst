﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Demo_Manage.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core;



namespace Demo_Manage.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        /*public ActionResult UploadImage()
        {
            return View();
        }

        public string ProcessUpload(HttpPostAttribute file)
        {
            // xử lý upload
            file.SaveAs(ServerAddress.MapPath("~/wwwroot/images"+file.FileName));            
            return "/wwwroot/images/" + file.FileName;    
        }*/
    }
}