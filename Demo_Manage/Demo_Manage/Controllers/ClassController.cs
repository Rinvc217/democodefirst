﻿using Demo_Manage.Models;
using Demo_Manage.Service.ClassService;
using Microsoft.AspNetCore.Mvc;

namespace Demo_Manage.Controllers
{
    public class ClassController : Controller
    {
        private IClassService _classService;

        public ClassController(IClassService classService)
        {
            _classService = classService;
        }
        
        // Get:Class/
        [HttpGet]
        public IActionResult Index()
        {
            return View(_classService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Class clas)
        {
            _classService.Add(clas);
            _classService.Save();
            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _classService.GetById(id);
            return View(getId);
        }
        
        [HttpPost]
        public IActionResult Edit(Class clas)
        {
            _classService.Update(clas);
            _classService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _classService.Delete(id);
            _classService.Save();
            return RedirectToAction("Index");
        }
    }
}