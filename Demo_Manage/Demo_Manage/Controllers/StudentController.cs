﻿using Demo_Manage.Models;
using Demo_Manage.Reponsitory.StudentReponsitory;
using Demo_Manage.Service.StudentService;
using Microsoft.AspNetCore.Mvc;
using Demo_Manage.Service.StudentService;
namespace Demo_Manage.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;

        // Constant giá trị của nó không thể thay đổi trong suốt chương trình/
        // Readonly cũng tương tự như Constant nhưng điểm khác biệt là
        // các bạn có thể thay đổi giá trị của nó nhưng chỉ được phép thay đổi trong Contructor mà thôi.
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        
        // Get: //Student/
        [HttpGet]
        public IActionResult Index()
        {
            return View(_studentService.GetAll());
            
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            _studentService.Add(student);
            _studentService.save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _studentService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            _studentService.Update(student);
            _studentService.save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _studentService.Delete(id);
            _studentService.save();
            return RedirectToAction("Index");
        }
    }
}
// * : Nếu ta muốn trả về View cho trình duyệt, ta gọi phương thức View ().
// Nếu ta muốn chuyển hướng người dùng từ một hành động điều khiển sang hành động khác,
// ta gọi phương thức RedirectToAction ().

// * :Trong hầu hết các trường hợp, một hành động của bộ điều khiển trả về ViewResult. 